<?php

namespace App\Entity;

use App\Repository\PasantiaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PasantiaRepository::class)
 */
class Pasantia
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaInicio;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaFin;


   

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $estadoPasantia;


    /**
     * @ORM\Column(type="date")
     */
    private $fechaInicioTramite;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaFinTramite;


  

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnviarinformaciondePasantia;

    

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;


    /**
     * @ORM\Column(type="integer")
     */
    private $pasos;

   

    /**
     * @ORM\Column(type="boolean")
     */
    private $issolicitarAprobaciondePasantia;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isalizarSolicitudRector;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaUltimaModificacion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $motivoRechazo;

    /**
     * @ORM\ManyToOne(targetEntity=AreaUnRaf::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $areaActual;

    /**
     * @ORM\ManyToOne(targetEntity=AreaUnRaf::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $areaEncargada;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAbrirExpedientePasantia;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInformarAprobacionaEmpresa;

    /**
     * @ORM\ManyToOne(targetEntity=Convenio::class, inversedBy="pasantia")
     */
    private $convenioPasantia;

    /**
     * @ORM\ManyToMany(targetEntity=Pasante::class, inversedBy="pasantias")
     */
    private $pasante;

    /**
     * @ORM\OneToMany(targetEntity=Pago::class, mappedBy="pasantia")
     */
    private $pago;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaModificacion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ultimoUsuario;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isNotaSolicituddePasante;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isConstanciadeIncripcionenAfip;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDDJJ;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnviosolicitudRector;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isIniciarConvocatoria;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isenviarNomina;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isenviarDocumentacio;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInformeSeguimientoPasantia;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnviarActasdeCompromiso;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRecibirActasdeCompromiso;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaInicioPasantia;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fechaFinPasantia;


    public function __construct()
    {
        $this->pasante = new ArrayCollection();
        $this->pago = new ArrayCollection();
    }

    


    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaInicio(): ?\DateTimeInterface
    {
        return $this->fechaInicio;
    }

    public function setFechaInicio(\DateTimeInterface $fechaInicio): self
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    public function getFechaFin(): ?\DateTimeInterface
    {
        return $this->fechaFin;
    }

    public function setFechaFin(\DateTimeInterface $fechaFin): self
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }


    

    public function getEstadoPasantia(): ?string
    {
        return $this->estadoPasantia;
    }

    public function setEstadoPasantia(string $estadoPasantia): self
    {
        $this->estadoPasantia = $estadoPasantia;

        return $this;
    }


    

    public function getFechaInicioTramite(): ?\DateTimeInterface
    {
        return $this->fechaInicioTramite;
    }

    public function setFechaInicioTramite(\DateTimeInterface $fechaInicioTramite): self
    {
        $this->fechaInicioTramite = $fechaInicioTramite;

        return $this;
    }

    public function getFechaFinTramite(): ?\DateTimeInterface
    {
        return $this->fechaFinTramite;
    }

    public function setFechaFinTramite(\DateTimeInterface $fechaFinTramite): self
    {
        $this->fechaFinTramite = $fechaFinTramite;

        return $this;
    }


    

    public function getIsEnviarinformaciondePasantia(): ?bool
    {
        return $this->isEnviarinformaciondePasantia;
    }

    public function setIsEnviarinformaciondePasantia(bool $isEnviarinformaciondePasantia): self
    {
        $this->isEnviarinformaciondePasantia = $isEnviarinformaciondePasantia;

        return $this;
    }

    

    

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    

    public function getPasos(): ?int
    {
        return $this->pasos;
    }

    public function setPasos(int $pasos): self
    {
        $this->pasos = $pasos;

        return $this;
    }

   

    public function getIssolicitarAprobaciondePasantia(): ?bool
    {
        return $this->issolicitarAprobaciondePasantia;
    }

    public function setIssolicitarAprobaciondePasantia(bool $issolicitarAprobaciondePasantia): self
    {
        $this->issolicitarAprobaciondePasantia = $issolicitarAprobaciondePasantia;

        return $this;
    }

    public function getIsalizarSolicitudRector(): ?bool
    {
        return $this->isalizarSolicitudRector;
    }

    public function setIsalizarSolicitudRector(bool $isalizarSolicitudRector): self
    {
        $this->isalizarSolicitudRector = $isalizarSolicitudRector;

        return $this;
    }

    public function getFechaUltimaModificacion(): ?\DateTimeInterface
    {
        return $this->fechaUltimaModificacion;
    }

    public function setFechaUltimaModificacion(\DateTimeInterface $fechaUltimaModificacion): self
    {
        $this->fechaUltimaModificacion = $fechaUltimaModificacion;

        return $this;
    }

    public function getMotivoRechazo(): ?string
    {
        return $this->motivoRechazo;
    }

    public function setMotivoRechazo(?string $motivoRechazo): self
    {
        $this->motivoRechazo = $motivoRechazo;

        return $this;
    }

    public function getAreaActual(): ?AreaUnRaf
    {
        return $this->areaActual;
    }

    public function setAreaActual(?AreaUnRaf $areaActual): self
    {
        $this->areaActual = $areaActual;

        return $this;
    }

    public function getAreaEncargada(): ?AreaUnRaf
    {
        return $this->areaEncargada;
    }

    public function setAreaEncargada(?AreaUnRaf $areaEncargada): self
    {
        $this->areaEncargada = $areaEncargada;

        return $this;
    }

    public function getIsAbrirExpedientePasantia(): ?bool
    {
        return $this->isAbrirExpedientePasantia;
    }

    public function setIsAbrirExpedientePasantia(bool $isAbrirExpedientePasantia): self
    {
        $this->isAbrirExpedientePasantia = $isAbrirExpedientePasantia;

        return $this;
    }

    public function getIsInformarAprobacionaEmpresa(): ?bool
    {
        return $this->isInformarAprobacionaEmpresa;
    }

    public function setIsInformarAprobacionaEmpresa(bool $isInformarAprobacionaEmpresa): self
    {
        $this->isInformarAprobacionaEmpresa = $isInformarAprobacionaEmpresa;

        return $this;
    }

    public function getConvenioPasantia(): ?Convenio
    {
        return $this->convenioPasantia;
    }

    public function setConvenioPasantia(?Convenio $convenioPasantia): self
    {
        $this->convenioPasantia = $convenioPasantia;

        return $this;
    }

    /**
     * @return Collection|Pasante[]
     */
    public function getPasante(): Collection
    {
        return $this->pasante;
    }

    public function addPasante(Pasante $pasante): self
    {
        if (!$this->pasante->contains($pasante)) {
            $this->pasante[] = $pasante;
        }

        return $this;
    }

    public function removePasante(Pasante $pasante): self
    {
        $this->pasante->removeElement($pasante);

        return $this;
    }

    /**
     * @return Collection|Pago[]
     */
    public function getPago(): Collection
    {
        return $this->pago;
    }

    public function addPago(Pago $pago): self
    {
        if (!$this->pago->contains($pago)) {
            $this->pago[] = $pago;
            $pago->setPasantia($this);
        }

        return $this;
    }

    public function removePago(Pago $pago): self
    {
        if ($this->pago->removeElement($pago)) {
            // set the owning side to null (unless already changed)
            if ($pago->getPasantia() === $this) {
                $pago->setPasantia(null);
            }
        }

        return $this;
    }

    public function getFechaModificacion(): ?\DateTimeInterface
    {
        return $this->fechaModificacion;
    }

    public function setFechaModificacion(\DateTimeInterface $fechaModificacion): self
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    public function getUltimoUsuario(): ?string
    {
        return $this->ultimoUsuario;
    }

    public function setUltimoUsuario(string $ultimoUsuario): self
    {
        $this->ultimoUsuario = $ultimoUsuario;

        return $this;
    }

    public function getIsNotaSolicituddePasante(): ?bool
    {
        return $this->isNotaSolicituddePasante;
    }

    public function setIsNotaSolicituddePasante(bool $isNotaSolicituddePasante): self
    {
        $this->isNotaSolicituddePasante = $isNotaSolicituddePasante;

        return $this;
    }

    public function getIsConstanciadeIncripcionenAfip(): ?bool
    {
        return $this->isConstanciadeIncripcionenAfip;
    }

    public function setIsConstanciadeIncripcionenAfip(bool $isConstanciadeIncripcionenAfip): self
    {
        $this->isConstanciadeIncripcionenAfip = $isConstanciadeIncripcionenAfip;

        return $this;
    }

    public function getIsDDJJ(): ?bool
    {
        return $this->isDDJJ;
    }

    public function setIsDDJJ(bool $isDDJJ): self
    {
        $this->isDDJJ = $isDDJJ;

        return $this;
    }

    public function getIsEnviosolicitudRector(): ?bool
    {
        return $this->isEnviosolicitudRector;
    }

    public function setIsEnviosolicitudRector(bool $isEnviosolicitudRector): self
    {
        $this->isEnviosolicitudRector = $isEnviosolicitudRector;

        return $this;
    }

    public function getIsIniciarConvocatoria(): ?bool
    {
        return $this->isIniciarConvocatoria;
    }

    public function setIsIniciarConvocatoria(bool $isIniciarConvocatoria): self
    {
        $this->isIniciarConvocatoria = $isIniciarConvocatoria;

        return $this;
    }

    public function getIsenviarNomina(): ?bool
    {
        return $this->isenviarNomina;
    }

    public function setIsenviarNomina(bool $isenviarNomina): self
    {
        $this->isenviarNomina = $isenviarNomina;

        return $this;
    }

    public function getIsenviarDocumentacio(): ?bool
    {
        return $this->isenviarDocumentacio;
    }

    public function setIsenviarDocumentacio(bool $isenviarDocumentacio): self
    {
        $this->isenviarDocumentacio = $isenviarDocumentacio;

        return $this;
    }

    public function getIsInformeSeguimientoPasantia(): ?bool
    {
        return $this->isInformeSeguimientoPasantia;
    }

    public function setIsInformeSeguimientoPasantia(bool $isInformeSeguimientoPasantia): self
    {
        $this->isInformeSeguimientoPasantia = $isInformeSeguimientoPasantia;

        return $this;
    }

    public function getIsEnviarActasdeCompromiso(): ?bool
    {
        return $this->isEnviarActasdeCompromiso;
    }

    public function setIsEnviarActasdeCompromiso(bool $isEnviarActasdeCompromiso): self
    {
        $this->isEnviarActasdeCompromiso = $isEnviarActasdeCompromiso;

        return $this;
    }

    public function getIsRecibirActasdeCompromiso(): ?bool
    {
        return $this->isRecibirActasdeCompromiso;
    }

    public function setIsRecibirActasdeCompromiso(bool $isRecibirActasdeCompromiso): self
    {
        $this->isRecibirActasdeCompromiso = $isRecibirActasdeCompromiso;

        return $this;
    }

    public function getFechaInicioPasantia(): ?\DateTimeInterface
    {
        return $this->fechaInicioPasantia;
    }

    public function setFechaInicioPasantia(?\DateTimeInterface $fechaInicioPasantia): self
    {
        $this->fechaInicioPasantia = $fechaInicioPasantia;

        return $this;
    }

    public function getFechaFinPasantia(): ?\DateTimeInterface
    {
        return $this->fechaFinPasantia;
    }

    public function setFechaFinPasantia(?\DateTimeInterface $fechaFinPasantia): self
    {
        $this->fechaFinPasantia = $fechaFinPasantia;

        return $this;
    }

    
   
}
