<?php

namespace App\Form;

use App\Entity\Convenio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\AreaUnRaf;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 

class ArchivoConvenioPaso5Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('ultimaArea', EntityType::class,[
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
        ] )
       
        ->add('isEnviarArchivoExpediente',null,['required'=>true,'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'isEnviarArchivoExpediente')])
        ->add('isEnviarArchivoDespacho',null,['required'=>true,'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'isEnviarArchivoDespacho')])
        ->add('isEnviarArchivoOrganizacion',null,['required'=>true,'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'isEnviarArchivoOrganizacion')])
        
        ->add('areaSiguiente', EntityType::class,[
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
        ] )

        ->add('Siguiente',SubmitType::class)
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Convenio::class,
        ]);
    }
}
