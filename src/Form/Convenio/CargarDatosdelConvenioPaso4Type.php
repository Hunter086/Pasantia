<?php

namespace App\Form;

use App\Entity\Convenio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\AreaUnRaf;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class CargarDatosdelConvenioPaso4Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('ultimaArea', EntityType::class,[
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'UltimaArea')
        ] )
        ->add('fechaRenovacion', DateType::class, [
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'fechaRenovacion'),
            'widget' => 'single_text'
        ])
        ->add('fechaInicio', DateType::class, [
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'fechaInicio'),
            'widget' => 'single_text'
        ])
        ->add('fechaFin', DateType::class, [
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'fechaFin'),
            'widget' => 'single_text'
        ])
        ->add('isRenovacionAutomatica', ChoiceType::class, [
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'isRenovacionAutomatica'),
            'choices'  => [
                '' => '',
                'Convenio con renovación Automática' => 'AUTOMATICO',
                'Convenio sin renovación Automática' => 'MANUAL'
            ],
        ])
        ->add('documentoConvenio', FileType::class, [
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'fechaFin'),
            'label' => 'Seleccione un Documento (PDF file)',

            // unmapped means that this field is not associated to any entity property
            'mapped' => false,

            // make it optional so you don't have to re-upload the PDF file
            // every time you edit the Product details
            'required' => true,

            // unmapped fields can't define their validation using annotations
            // in the associated entity, so you can use the PHP constraint classes
            'constraints' => [
                new File([
                    'maxSize' => '1024k',
                    'mimeTypes' => [
                        'application/pdf',
                        'application/x-pdf',
                    ],
                    'mimeTypesMessage' => 'Please upload a valid PDF document',
                ])
            ],
        ])

        ->add('areaSiguiente', EntityType::class,[
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'UltimaArea')
        ] )
        
        
        ->add('Siguiente',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Convenio::class,
        ]);
    }
}
