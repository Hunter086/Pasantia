<?php

namespace App\Form;

use App\Entity\Convenio;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\AreaUnRaf;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ImputacionConvenioPaso6Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('ultimaArea', EntityType::class,[
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
        ] )
       
        ->add('imputacionConvenio',null,['required'=>true,'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'imputacionConvenio')])
        
        ->add('areaSiguiente', EntityType::class,[
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
        ] )

        ->add('Siguiente',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Convenio::class,
        ]);
    }
}
