<?php

namespace App\Form;

use App\Entity\Pasantia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\AreaUnRaf;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class SolicitarAprobaciondepasantiaPaso3Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('areaActual', EntityType::class,[
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Area Encargada')
        ] )
        
        
        ->add('isalizarSolicitudRector', ChoiceType::class, [
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Area Encargada'),
            'choices'  => [
                '' => '',
                'Pasantía Aceptada' => true,
                'Pasantía Rechazada' => false,
            ],
        ])
        ->add('motivoRechazo', TextareaType::class, [
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Motivo'),
            'required' => false,
            ])

        ->add('areaEncargada', EntityType::class,[
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Area Encargada'),
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Area Siguiente')
        ] )
        ->add('Siguiente',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pasantia::class,
        ]);
    }
}
