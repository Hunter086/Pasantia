<?php

namespace App\Form;

use App\Entity\Pasantia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BuscarPasanteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('search', TextType::class, [
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Buscar Pasante'),
            'required' => false,
            ])
        ->add('Buscar',SubmitType::class, [
            'attr' => array('class' => 'btn btn-outline-info text-left mt-2', 'placeholder' => 'Buscar'),
            
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        
    }
}
