<?php

namespace App\Form;

use App\Entity\Pasantia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType; 
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\AreaUnRaf;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class RecibiryVerificarDocumentacionPaso2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('areaActual', EntityType::class,[
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Area Encargada')
        ] )
        ->add('isNotaSolicituddePasante',null,['required'=>true,'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'NotaSolicituddePasante')])
        ->add('isConstanciadeIncripcionenAfip',null,['required'=>true,'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'ConstanciadeIncripcionenAfip')])
        ->add('isDDJJ',null,['required'=>true,'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'DDJJ')])
        ->add('isEnviosolicitudRector',null,['required'=>true,'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Envio solicitud Rector')])
        
        ->add('areaEncargada', EntityType::class,[
            'class'=> AreaUnRaf::class,
            'choice_label'=>'nombre',
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Area Siguiente')
        ] )
        ->add('Siguiente',SubmitType::class,[
            'attr' => array('class' => 'form-control camposEstandar', 'placeholder' => 'Area Siguiente')
        ] )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pasantia::class,
        ]);
    }
}
