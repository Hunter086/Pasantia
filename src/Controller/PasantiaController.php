<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Pasantia;
use App\Entity\Pasante;
use App\Entity\Pago;
use App\Form\ActaCompromisoPaso9Type;



use Symfony\Component\HttpFoundation\Request;

use App\Form\EnviarInformacionPasantiaPaso1Type;
use App\Form\RecibiryVerificarDocumentacionPaso2Type;
use App\Form\SolicitarAprobaciondepasantiaPaso3Type;
use App\Form\AbrirExpedientePasantiaPaso4Type;
use App\Form\ContactoOrganizacionPaso5Type;
use App\Form\CargarConvenioPaso6Type;
use App\Form\InicioConvocatoriaPaso7Type;
use App\Form\EnviodeNominasyDocumentacionPaso8Type;
use App\Form\CargarPasanteType;
use App\Form\BuscarPasanteType;


class PasantiaController extends AbstractController
{
    /**
     * @Route("/pasantia", name="pasantia")
     */
    public function index(): Response
    {
        return $this->render('pasantia/index.html.twig', [
            'controller_name' => 'PasantiaController',
        ]);
    }
    /**
        * @Route("/nuevaPasantia", name="nuevaPasantia")
    */
    public function nuevaPasantia(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager= $this->getDoctrine()->getManager();
        $dateTime = new \DateTime();
        $fechaActual = $dateTime->format('d-m-Y');
        $username = $this->getUser();
        $pasantia = new Pasantia();
        $formulario = $this->createForm(EnviarInformacionPasantiaPaso1Type::class, $pasantia);
        $formulario->handleRequest($request);

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            try {
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasantia->setPasos(2);
                $pasantia->setFechaInicio($dateTime);
                $pasantia->setFechaFin($dateTime);
                $pasantia->setFechaInicioTramite($dateTime);
                $pasantia->setFechaFinTramite($dateTime);
                $pasantia->setfechaUltimaModificacion($dateTime);
                #Setear Pasos Siguientes 
                
                
                $pasantia->setEstadoPasantia('En Proceso');
                $pasantia->setIsNotaSolicituddePasante(false);
                $pasantia->setIsConstanciadeIncripcionenAfip(false);
                $pasantia->setIsDDJJ(false);
                $pasantia->setIsEnviosolicitudRector(false);
                $pasantia->setIssolicitarAprobaciondePasantia(false);
                $pasantia->setIsalizarSolicitudRector(false);
                $pasantia->setIsAbrirExpedientePasantia(false);
                $pasantia->setIsInformarAprobacionaEmpresa(false);
                $pasantia->setIsIniciarConvocatoria(false);
                $pasantia->setIsenviarNomina(false);
                $pasantia->setIsenviarDocumentacio(false);

                $pasantia->setIsEnviarActasdeCompromiso(false);
                $pasantia->setIsRecibirActasdeCompromiso(false);

                $pasantia->setIsInformeSeguimientoPasantia(false);

                $manager->persist($pasantia);
                $manager->flush();
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->redirectToRoute('nuevaPasantia');
            }                     
            

            $this -> addFlash('info', '¡Documentación recibida correctamente!');
            return $this->redirectToRoute('recibiryVerificarDocumentacion',['id'=> $pasantia->getId()]);
        }

        return $this->render('pasantia/enviarInformaciondePasantiPaso1.html.twig', [
            'formulario' => $formulario->createView(),
        ]);
    }
    /**
     * @Route("/recibiryVerificarDocumentacion/{id}", name="recibiryVerificarDocumentacion")
     */
    public function recibiryVerificarDocumentacion(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            $formulario = $this->createForm(RecibiryVerificarDocumentacionPaso2Type::class,$pasantia);
            $formulario->handleRequest($request);
            $pasantia->setPasos(2);
            $manager->flush();

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            try {
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasantia->setPasos(3);
                $pasantia->setfechaUltimaModificacion($dateTime);
                
                $manager->persist($pasantia);
                $manager->flush();
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->render('pasantia/recibiryVerificarDocumentacionPaso2.html.twig', [
                    'formulario' => $formulario->createView(),'pasantia' => $pasantia,
                ]);
            }                     
            

            $this -> addFlash('info', '¡Datos cargados exitosamente!');
            return $this->redirectToRoute('solicitarAprobaciondepasantia',['id'=> $pasantia->getId()]);
        }

        return $this->render('pasantia/recibiryVerificarDocumentacionPaso2.html.twig', [
            'formulario' => $formulario->createView(),
        ]);
    }
    /**
     * @Route("/solicitarAprobaciondepasantia/{id}", name="solicitarAprobaciondepasantia")
     */
    public function solicitarAprobaciondepasantia(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            $formulario = $this->createForm(SolicitarAprobaciondepasantiaPaso3Type ::class,$pasantia);
            $formulario->handleRequest($request);
            $pasantia->setPasos(3);
            $manager->flush();

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            try {
                if(!$pasantia->getIsalizarSolicitudRector()){
                    $pasantia->setEstadoPasantia('Rechazada');
                }
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasantia->setfechaUltimaModificacion($dateTime);
                $pasantia->setPasos(4);
                
                $manager->persist($pasantia);
                $manager->flush();
                if($pasantia->getEstadoPasantia()=='Rechazada'){
                    
                    return $this->redirectToRoute('pasantiaRechazada',['id'=> $pasantia->getId()]);
                }
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->render('pasantia/solicitarAprobaciondepasantiaPaso3.html.twig', [
                    'formulario' => $formulario->createView(),'pasantia' => $pasantia,
                ]);
            }                     
            
            
            $this -> addFlash('info', '¡Datos cargados exitosamente!');
            return $this->redirectToRoute('abrirExpedientePasantia',['id'=> $pasantia->getId()]);
        }

        return $this->render('pasantia/solicitarAprobaciondepasantiaPaso3.html.twig', [
            'formulario' => $formulario->createView(),'pasantia' => $pasantia,
        ]);
    }
    /**
     * @Route("/pasantiaRechazada/{id}", name="pasantiaRechazada")
     */
    public function pasantiaRechazada(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();

        $pasantia= $manager->getRepository(Pasantia::class)->find($id);
        return $this->render('pasantia/verDatosPasantiaRechazada.html.twig', ['pasantia' => $pasantia]
            );
    }
    /**
     * @Route("/abrirExpedientePasantia/{id}", name="abrirExpedientePasantia")
     */
    public function abrirExpedientePasantia(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            $formulario = $this->createForm(AbrirExpedientePasantiaPaso4Type::class,$pasantia);
            $formulario->handleRequest($request);
            $pasantia->setPasos(4);
            $manager->flush();

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            try {
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasantia->setfechaUltimaModificacion($dateTime);
                $pasantia->setPasos(5);
                
                $manager->persist($pasantia);
                $manager->flush();
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->render('pasantia/abrirExpedientePasantiaPaso4.html.twig', [
                    'formulario' => $formulario->createView(),'pasantia' => $pasantia,
                ]);
            }                     
            

            $this -> addFlash('info', '¡Datos cargados exitosamente!');
            return $this->redirectToRoute('contactoOrganizacion',['id'=> $pasantia->getId()]);
        }

        return $this->render('pasantia/abrirExpedientePasantiaPaso4.html.twig', [
            'formulario' => $formulario->createView(),'pasantia' => $pasantia,
        ]);
    }
    /**
     * @Route("/contactoOrganizacion/{id}", name="contactoOrganizacion")
     */
    public function contactoOrganizacion(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            $formulario = $this->createForm(ContactoOrganizacionPaso5Type::class,$pasantia);
            $formulario->handleRequest($request);
            $pasantia->setPasos(5);
            $manager->flush();

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            try {
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasantia->setfechaUltimaModificacion($dateTime);
                $pasantia->setPasos(6);
                
                
                $manager->persist($pasantia);
                $manager->flush();
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->render('pasantia/contactoOrganizacionPaso5.html.twig', [
                    'formulario' => $formulario->createView(),'pasantia' => $pasantia,
                ]);
            }                     
            

            $this -> addFlash('info', '¡Datos cargados exitosamente!');
            return $this->redirectToRoute('cargarConvenio',['id'=> $pasantia->getId()]);
        }

        return $this->render('pasantia/contactoOrganizacionPaso5.html.twig', [
            'formulario' => $formulario->createView(),'pasantia' => $pasantia,
        ]);
    }
     /**
     * @Route("/cargarConvenio/{id}", name="cargarConvenio")
     */
    public function cargarConvenio(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            $formulario = $this->createForm(CargarConvenioPaso6Type::class,$pasantia);
            $formulario->handleRequest($request);
            $pasantia->setPasos(6);
            $manager->flush();

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            try {
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasantia->setfechaUltimaModificacion($dateTime);
                $pasantia->setPasos(7);
                
                $manager->persist($pasantia);
                $manager->flush();
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->render('pasantia/cargarConvenioPaso6.html.twig', [
                    'formulario' => $formulario->createView(),'pasantia' => $pasantia,
                ]);
            }                     
            

            $this -> addFlash('info', '¡Datos cargados exitosamente!');
            return $this->redirectToRoute('iniciarConvocatoria',['id'=> $pasantia->getId()]);
        }

        return $this->render('pasantia/cargarConvenioPaso6.html.twig', [
            'formulario' => $formulario->createView(),'pasantia' => $pasantia,
        ]);
    }
    /**
     * @Route("/iniciarConvocatoria/{id}", name="iniciarConvocatoria")
     */
    public function iniciarConvocatoria(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            
            $pasantia->setPasos(7);
            $manager->flush();

            $formulario = $this->createForm(InicioConvocatoriaPaso7Type::class,$pasantia);
            $formulario->handleRequest($request);

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            try {
                
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasantia->setfechaUltimaModificacion($dateTime);
                $pasantia->setPasos(8);
           
                $manager->persist($pasantia);
                $manager->flush();
                
            $this -> addFlash('info', '¡Datos cargados exitosamente!');
            return $this->redirectToRoute('enviodeNominasyDocumentacion',['id'=> $pasantia->getId()]);
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->render('pasantia/iniciarConvocatoriaPaso7.html.twig', [
                    'formulario' => $formulario->createView(),'pasantia'=> $pasantia]);
            }                     
            

        }

        return $this->render('pasantia/iniciarConvocatoriaPaso7.html.twig', [
            'formulario' => $formulario->createView(),'pasantia'=> $pasantia]);
    }

    /**
     * @Route("/enviodeNominasyDocumentacion/{id}", name="enviodeNominasyDocumentacion")
     */
    public function enviodeNominasyDocumentacion(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            
            $pasantia->setPasos(8);
            $manager->flush();

            $formulario = $this->createForm(EnviodeNominasyDocumentacionPaso8Type::class,$pasantia);
            $formulario->handleRequest($request);

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            try {
                
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasantia->setfechaUltimaModificacion($dateTime);
                $pasantia->setPasos(9);
            
                $manager->persist($pasantia);
                $manager->flush();
                
            $this -> addFlash('info', '¡Datos cargados exitosamente!');
            return $this->redirectToRoute('actaCompromiso',['id'=> $pasantia->getId()]);
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->render('pasantia/EnviodeNominasyDocumentacionPaso8.html.twig', [
                    'formulario' => $formulario->createView(),'pasantia'=> $pasantia]);
            }                     
            

        }

        return $this->render('pasantia/EnviodeNominasyDocumentacionPaso8.html.twig', [
        'formulario' => $formulario->createView(),'pasantia'=> $pasantia]);
    }
    /**
     * @Route("/actaCompromiso/{id}", name="actaCompromiso")
     */
    public function actaCompromiso(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            
            $pasantia->setPasos(9);
            $manager->flush();

            $formulario = $this->createForm(ActaCompromisoPaso9Type::class,$pasantia);
            $formulario->handleRequest($request);

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            
            try {
                if($formulario->get('isRecibirActasdeCompromiso')->getData()){
                    $pasantia->setPasos(10);     
                    $pasantia->setFechaModificacion($dateTime);
                    $pasantia->setUltimoUsuario("".$username->getUsername());
                    $pasantia->setfechaUltimaModificacion($dateTime);
                    
                    $manager->persist($pasantia);
                    $manager->flush();
                    
                    $this -> addFlash('info', '¡Datos cargados exitosamente!');
                    return $this->redirectToRoute('cargarPasante',['id'=> $pasantia->getId()]);
                }else{
                    $pasantia->setIsRecibirActasdeCompromiso(false);
                    $manager->flush();
                    return $this->render('pasantia/actaCompromisoPaso9.html.twig', [
                        'formulario' => $formulario->createView(),'pasantia'=> $pasantia]);
                }
                
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->render('pasantia/actaCompromisoPaso9.html.twig', [
                    'formulario' => $formulario->createView(),'pasantia'=> $pasantia]);
            }                     
            

        }

        return $this->render('pasantia/actaCompromisoPaso9.html.twig', [
        'formulario' => $formulario->createView(),'pasantia'=> $pasantia]);
    }
    
    /**
     * @Route("/cargarPasante/{id}", name="cargarPasante")
     */
    public function cargarPasante(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            $pasante= $manager->getRepository(Pasante::class)->findByestadoPasante('Inactivo');

            $pasantesPasantia= $pasantia->getPasante();

            $formulario = $this->createForm(CargarPasanteType::class,$pasantia);
            
            $formularioBuscador = $this->createForm(BuscarPasanteType::class);
            $formularioBuscador->handleRequest($request);
            $formulario->handleRequest($request);
            $pasantia->setPasos(10);
            $manager->flush();
            if ($formularioBuscador->isSubmitted() && $formularioBuscador->isValid()) {
                return $this->buscar($formularioBuscador, $manager, $formulario, $pasantia);
                
            }
        if ($formulario->isSubmitted() && $formulario->isValid()) {
            try {
                if(count($pasantesPasantia)==0 || $pasantesPasantia== null ){
                    $this -> addFlash('error', '¡No ingreso datos a la tabla!');
                    return $this->render('pasantia/cargarPasantePaso9.html.twig', ['formularioBuscador' =>$formularioBuscador->createView(),
                        'formulario' => $formulario->createView(),'pasantia'=> $pasantia,'pasante'=>  $pasante]);
                }
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasantia->setfechaUltimaModificacion($dateTime);
                $pasantia->setPasos(11);
                $manager->persist($pasantia);
                $manager->flush();
                
            $this -> addFlash('info', '¡Datos cargados exitosamente!');
            return $this->redirectToRoute('finalizarCarga',['id'=> $pasantia->getId()]);
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->render('pasantia/cargarPasantePaso10.html.twig', ['formularioBuscador' =>$formularioBuscador->createView(),
                    'formulario' => $formulario->createView(),'pasantia'=> $pasantia,'pasante'=>  $pasante]);
            }                     
            

        }

        return $this->render('pasantia/cargarPasantePaso10.html.twig', ['formularioBuscador' =>$formularioBuscador->createView(),
            'formulario' => $formulario->createView(),'pasantia'=> $pasantia,'pasante'=>  $pasante]);
    }
    /**
     * buscador de palabras
     */
    public function buscar($formularioBuscador, $manager, $formulario, $pasantia)
    {
        try {
            $palabra= $formularioBuscador->get('search')->getData();
            $pasante = $manager->getRepository(Pasante::class)->createQueryBuilder('o')
            ->where('o.estadoPasante LIKE :estado and o.nombre LIKE :pasante or o.apellido LIKE :pasante or o.dni LIKE :pasante
            or o.legajo LIKE :pasante or o.cuil LIKE :pasante')
            ->setParameter('estado', 'Inactivo')
            ->setParameter('pasante', '%'.$palabra.'%')
            ->getQuery()
            ->getResult();
        } catch (\Throwable $th) {
            $this -> addFlash('error', '¡Error al buscar los datos!'.$th);
            return $this->render('pasantia/cargarPasantePaso10.html.twig', ['formularioBuscador' =>$formularioBuscador->createView(),
        'formulario' => $formulario->createView(),'pasantia'=> $pasantia,'pasante'=>  $pasante]);
        }
        return $this->render('pasantia/cargarPasantePaso10.html.twig', ['formularioBuscador' =>$formularioBuscador->createView(),
        'formulario' => $formulario->createView(),'pasantia'=> $pasantia,'pasante'=>  $pasante]);

    }


    /**
     * @Route("/agregarPasantedePasantia/{id}/{idPasante}", name="agregarPasantedePasantia")
     */
    public function agregarPasantedePasantia(Request $request, $id, $idPasante)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $username = $this->getUser();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            $pasante= $manager->getRepository(Pasante::class)->find($idPasante);
            $formulario = $this->createForm(CargarPasanteType::class,$pasantia);
            $formulario->handleRequest($request);
            

            try {
                $pasantia->setFechaModificacion($dateTime);
                $pasantia->setUltimoUsuario("".$username->getUsername());
                $pasante->setFechaSeguimiento($dateTime);
                $pasante->setIsSeguimientodelMes(false);
                $pasante->setIsInformeSeguimiento('No Realizado');
                $pasante->setEstadoPasante('Activo');
                $pasantia->addPasante($pasante);
                
                $manager->persist($pasantia);
                $manager->flush();
                $this -> addFlash('info', '¡Pasante Agregado exitosamente!');
                return $this->redirectToRoute('cargarPasante',['id'=> $pasantia->getId()]);
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->redirectToRoute('cargarPasante',['id'=> $pasantia->getId()]);
            }                     
        

            $this -> addFlash('info', '¡Pasante Agregado exitosamente!');
                return $this->redirectToRoute('cargarPasante',['id'=> $pasantia->getId()]);
    }
    /**
     * @Route("/eliminarPasanteCargado/{id}/{idPasante}", name="eliminarPasanteCargado")
     */
    public function eliminarPasanteCargado(Request $request, $id, $idPasante)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();
        
        
        $pasante= $manager->getRepository(Pasante::class)->find($idPasante);
        $pasantia= $manager->getRepository(Pasantia::class)->find($id);
        
       try {

            $pasante->setEstadoPasante('Inactivo');
            $pasante->removePasantia($pasantia);
            $pasantia->removePasante($pasante);
            $manager->flush();
            $this -> addFlash('info', '¡El pasante se ha eliminado correctamente!');
            return $this->cargarPasante($request,$id);
       } catch (\Throwable $th) {
        $this -> addFlash('error', '¡Error al eliminar el pasante!'.$th);
        return $this->cargarPasante($request,$id);
       }
       return $this->cargarPasante($request,$id);
    }
    /**
     * @Route("/eliminarPasantedePasantia/{id}/{idPasante}", name="eliminarPasantedePasantia")
     */
    public function eliminarPasantedePasantia(Request $request, $id, $idPasante)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();
        
        
        $pasante= $manager->getRepository(Pasante::class)->find($idPasante);
        $pasantia= $manager->getRepository(Pasantia::class)->find($id);
        
       try {
            if($pasante->getIsSeguimientodelMes()==false){
            $this -> addFlash('error', '¡Error aún quedan seguimientos sin realizar!');
            return $this->verDatosPasantiaCargada($request,$id);
            
           }
           if($pasante->getIsInformeSeguimiento()=='No Realizado'){
            $this -> addFlash('error', '¡Error aún no ha realizado el Informe de Seguimiento!');
            return $this->verDatosPasantiaCargada($request,$id);
            
           }
            foreach ($pasante->getPago() as $pago) {
               if($pago->getEstadoPago()=='No Abonado' || $pago->getEstadoPago()=='Pagado' ){
                $this -> addFlash('error', '¡Error aún quedan pagos sin facturar!');
                return $this->verDatosPasantiaCargada($request,$id);
                
               }
            }
            $pasante->setEstadoPasante('Inactivo');
            $manager->flush();
            $this -> addFlash('info', '¡El pasante se ha eliminado correctamente!');
            return $this->verDatosPasantiaCargada($request,$id);
       } catch (\Throwable $th) {
        $this -> addFlash('error', '¡Error al eliminar el pasante!'.$th);
        return $this->verDatosPasantiaCargada($request,$id);
       }
       return $this->verDatosPasantiaCargada($request,$id);
    }
    /**
     * @Route("/finalizarCarga/{id}", name="finalizarCarga")
     */
    public function finalizarCarga(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            $manager=$this->getDoctrine()->getManager();
            $dateTime = new \DateTime();
            $pasantia= $manager->getRepository(Pasantia::class)->find($id);
            

            try {
                $pasantia->setfechaUltimaModificacion($dateTime);
                $pasantia->setPasos(11);
                $pasantia->setEstadoPasantia('Activa');
                
                $manager->persist($pasantia);
                $manager->flush();
               
                
            } catch (\Throwable $th) {
                $this -> addFlash('error', '¡Error al cargar los datos!'.$th);
                return $this->redirectToRoute('finalizarCarga',['id'=> $pasantia->getId()]);
            }                     
        

        return $this->render('pasantia/finalizarCargaPaso11.html.twig',['id'=> $pasantia->getId(),'pasantia'=> $pasantia]);
    }
     /**
     * @Route("verDatosPasantiaCargada/{id}", name="verDatosPasantiaCargada")
     */
    public function verDatosPasantiaCargada(Request $request, $id){
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();

        $pasantia= $manager->getRepository(Pasantia::class)->find($id);
        return $this->render('pasantia/verDatosPasantiaCargada.html.twig', ['pasantia' => $pasantia]
            );
     }
     /**
     * @Route("verDatosPasantiaInactiva/{id}", name="verDatosPasantiaInactiva")
     */
    public function verDatosPasantiaInactiva(Request $request, $id){
        
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();

        $pasantia= $manager->getRepository(Pasantia::class)->find($id);
        return $this->render('pasantia/verDatosPasantiaInactiva.html.twig', ['pasantia' => $pasantia]
            );
     }
     

    /**
     * @Route("/listarPasantiaActivas", name="listarPasantiaActivas")
     */
    public function listarPasantiaActivas(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();
        $pasantia= $manager->getRepository(Pasantia::class)->findByestadoPasantia('Activa');
        
        return $this->render('pasantia/listarPasantiaActiva.html.twig',
                ['pasantia' => $pasantia]
            );
    }
    /**
     * @Route("/listarPasantiaEnProceso", name="listarPasantiaEnProceso")
     */
    public function listarPasantiaEnProceso(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();
   
        
        $pasantia= $manager->getRepository(Pasantia::class)->findByestadoPasantia('En Proceso');
        
        return $this->render('pasantia/listarPasantiaEnProceso.html.twig',
                ['pasantia' => $pasantia]
            );
    }
    /**
     * @Route("/listarPasantiaRechazada", name="listarPasantiaRechazada")
     */
    public function listarPasantiaRechazada(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();
   
        
        $pasantia= $manager->getRepository(Pasantia::class)->findByestadoPasantia('Rechazada');
        
        return $this->render('pasantia/listarPasantiaRechazadas.html.twig',
                ['pasantia' => $pasantia]
            );
    }
    /**
     * @Route("/listarPasantiaInactiva", name="listarPasantiaInactiva")
     */
    public function listarPasantiaInactiva(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();
   
        
        $pasantia= $manager->getRepository(Pasantia::class)->findByestadoPasantia('Inactiva');
        
        return $this->render('pasantia/listarPasantiaInactiva.html.twig',
                ['pasantia' => $pasantia]
            );
    }
    
    /**
     * @Route("/listarPasantiarRenovar", name="listarPasantiarRenovar")
     */
    public function listarPasantiarRenovar(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();
   
        
        $pasantiaaRenovar= $manager->getRepository(Pasantia::class)->findall();
        $fecha_hoy = new \DateTime();

        $anio= $fecha_hoy->format("Y");
        $pasantia=array();
            if($pasantia!=null) {
                foreach ($pasantiaaRenovar as $pasantiaaRenovars) {
                    if($pasantiaaRenovars->getEstadoPasantia()=='Activa') {
            
                        $fecha_vencimiento_string= $pasantiaaRenovars->getFechaFin();
                        $fecha_vencimiento_string =  $fecha_vencimiento_string->format("d-m-Y");
                        $fecha_vencimiento= $pasantiaaRenovars->getFechaFin();
                        $anio_vencimiento=   $fecha_vencimiento->format("Y");
            
                        $fecha_vencimiento -> modify('-15 days');
                    
                        
                        if($fecha_vencimiento < $fecha_hoy &&  $anio_vencimiento=$anio){
                            array_push($pasantia, $pasantiaaRenovars );
                        }
                    }

                }
            }
            return $this->render('pasantia/listarPasantiaRenovar.html.twig',
            ['pasantia' => $pasantia]
        );
        
    }


        
       
    
    
    /**
     * @Route("/modificarPasantia/{id}", name="modificarPasantia")
     */
    
    public function modificarPasantia(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        $manager=$this->getDoctrine()->getManager();
        
        $Pasantia= $manager->getRepository(Pasantia::class)->find($id	);
        
        $form = $this->createForm(PasantiaType::class,$Pasantia);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()){
            
            $manager->flush();
            
            return $this->listarPasantiaInactiva($request);
            
        }
        
        return $this->render('pasantia/modificarPasantia.html.twig',
                ['formulario' => $form->createView()]
            );
    }
    /**
     * @Route("/eliminarPasantia/{id}", name="eliminarPasantia")
     */
    public function eliminarPasantia(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        
        $manager=$this->getDoctrine()->getManager();
        $pasantia= $manager->getRepository(Pasantia::class)->find($id);
        foreach ($pasantia->getPasante() as $pasante) {
            if($pasante->getEstadoPasante()=='Activo'){
                foreach ($pasantia->getPago() as $pago) {
                    if($pago->getEstadoPago()!='Facturado'){
                        $this -> addFlash('error', '¡Error aún quedan pagos sin Facturar!');
                        return $this->listarPasantiaActivas($request);
                    }
                }
                $this -> addFlash('error', '¡Error aún quedan pasantes activos!');
                return $this->listarPasantiaActivas($request);
            }
        }
        $pasantia->setEstadoPasantia('Inactiva');
        $manager->persist($pasantia);
        $manager->flush();
        $this -> addFlash('info', 'La Pasantia se ha  Inhabilitado correctamente');
        return $this->listarPasantiaInactiva($request);
        
    }
    /**
     * @Route("/eliminarPasantiaEnProceso/{id}", name="eliminarPasantiaEnProceso")
     */
    public function eliminarPasantiaEnProceso(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
        
        $manager=$this->getDoctrine()->getManager();
        $pasantia= $manager->getRepository(Pasantia::class)->find($id);
       try {
            if($pasantia->getEstadoPasantia()=='En Proceso'){
            $manager->remove($pasantia);
            $manager->flush();
            $this -> addFlash('info', 'La Pasantia se ha eliminado correctamente');
            return $this->listarPasantiaActivas($request);
        }
       } catch (\Throwable $th) {
        $this -> addFlash('error', 'Error en el sistema'.$th);
        return $this->listarPasantiaInactiva($request);
       }
           
        $this -> addFlash('error', 'La Pasantia no se puede eliminar porque contiene datos importantes');
        return $this->listarPasantiaInactiva($request);
        
    }

    /**
     * @Route("verPasantia/{id}", name="verPasantia")
     */
    
    public function verPasantia(Request $request, $id){
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }

        $manager=$this->getDoctrine()->getManager();
        
        $pasantia= $manager->getRepository(Pasantia::class)->find($id);

        if($pasantia->getEstadoPasantia()=='Rechazada'){
            return $this->redirectToRoute('pasantiaRechazada',['id'=> $pasantia->getId()]);
        }
        switch ($pasantia->getPasos()){
            case 2:
                
                return $this->redirectToRoute('recibiryVerificarDocumentacion',['id'=> $pasantia->getId()]);
                break;
            case 3:
               
                return $this->redirectToRoute('solicitarAprobaciondepasantia',['id'=> $pasantia->getId()]);
                break;
            case 4:
                
                return $this->redirectToRoute('abrirExpedientePasantia',['id'=> $pasantia->getId()]);
                break;
            case 5:
                
                return $this->redirectToRoute('contactoOrganizacion',['id'=> $pasantia->getId()]);
                break;
            case 6:
                
                return $this->redirectToRoute('cargarConvenio',['id'=> $pasantia->getId()]);
                break;
            case 7:
                
                return $this->redirectToRoute('iniciarConvocatoria',['id'=> $pasantia->getId()]);
                break;
            case 8:
                
                return $this->redirectToRoute('enviodeNominasyDocumentacion',['id'=> $pasantia->getId()]);
                break;
            case 9:
            
                return $this->redirectToRoute('actaCompromiso',['id'=> $pasantia->getId()]);
                break;
            case 10:
                
                return $this->redirectToRoute('cargarPasante',['id'=> $pasantia->getId()]);
                break;
            case 11:
            
                return $this->redirectToRoute('verDatosPasantiaCargada',['id'=> $pasantia->getId()]);
                break;
        }
        $this -> addFlash('error', '¡Error en el sistema, paso fuera de rango!');
        return $this->redirectToRoute('listarpasantia',['id'=> $pasantia->getId()]);
    }
}
