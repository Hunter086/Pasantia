<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Convenio;
use App\Entity\Pasantia;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;


class InicioController extends AbstractController
{
    /**
     * @Route("/inicio", name="inicio")
     */
    public function index()
    {
        $username = $this->getUser();

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this -> addFlash('error', '¡No tiene acceso a esta página!');
            return $this->redirect('https://intranet.unraf.edu.ar/');
        }
            
        

        $manager=$this->getDoctrine()->getManager();
   
        
        $convenio= $manager->getRepository(Convenio::class)->findAll();
        $pasantia= $manager->getRepository(Pasantia::class)->findAll();
        $fecha_hoy = new \DateTime();

        $anio= $fecha_hoy->format("Y");
        $this->seguimientoPasante($pasantia,  $fecha_hoy);
        $this->renovacionConvenios($convenio,  $fecha_hoy, $anio);
        $this->renovacionPasantia($pasantia,  $fecha_hoy, $anio);
        $this->vencimientoDePagos($pasantia,  $fecha_hoy, $anio);
        
        return $this->render('inicio/index.html.twig');
    }
    
     /**
     * 
     */
    public function seguimientoPasante($pasantia,  $fecha_hoy)
    {
        $diafecha= $fecha_hoy->format("j");
        if($pasantia!=null && $diafecha>= 17 && $diafecha<=5 ){
            foreach ($pasantia as $pasantias) {
                if($pasantias->getEstadoPasantia()=='Activa' && $pasantias->getIsInformeSeguimientoPasantia()==false) {
                    $pasantes= $pasantias->getPasante();
                    foreach($pasantes as $pasante) {
                        if($pasante->getIsInformeSeguimiento()=='No Realizado' && $pasante->getIsSeguimientodelMes()==false){
                            $newPageUrl = $this->generateUrl('verDatosPasantiaCargada', ['id' =>$pasantias->getId()]);
                            $this->addFlash('seguimientoPasanteMes',
                                sprintf('¡Falta el seguimiento de:  %s %s ! <a href="%s"><i class="far fa-eye"></i></a>',$pasante->getNombre(),$pasante->getApellido(),$newPageUrl)
                            );

                        }
                    }
                   
                }

             }
        }
    }
    /**
     * 
     */
    public function renovacionPasantia($pasantia,  $fecha_hoy, $anio)
    {
       

        if($pasantia!=null) {
            foreach ($pasantia as $pasantias) {
                if($pasantias-> getEstadoPasantia()=='Activa') {
           
                    $fecha_vencimiento_string= $pasantias->getFechaFinPasantia();
                    $fecha_vencimiento_string =  $fecha_vencimiento_string->format("d-m-Y");
                    $fecha_vencimiento= $pasantias->getFechaFinPasantia();
                    $anio_vencimiento=   $fecha_vencimiento->format("Y");
        
                    $fecha_vencimiento -> modify('-15 days');
                
                    
                    if($fecha_vencimiento <= $fecha_hoy &&  $anio_vencimiento=$anio){
                        $fecha_vencimiento = $fecha_vencimiento->format("d-m-Y");
                        $fecha_hoy = $fecha_hoy->format("d-m-Y");
                        $id= $pasantias->getId();
                        $newPageUrl = $this->generateUrl('verDatosPasantiaCargada', ['id' =>$pasantias->getId()]);
                        $this->addFlash('renovacionPasantia',
                            sprintf('Pasantía a punto de vencer! %s %s <a href="%s"><i class="far fa-eye"></i></a>',$pasantias->getConvenioPasantia()->getEmpresa()->getNombre(), $fecha_vencimiento_string, $newPageUrl)
                        );
                    }
                }

             }
        
        }
    }
    /**
     * 
     */
    public function vencimientoDePagos($pasantia,  $fecha_hoy, $anio)
    {
        
        if($pasantia!=null) {
            foreach ($pasantia as $pasantias) {
                if($pasantias-> getEstadoPasantia()=='Activa') {
                    foreach ($pasantias->getPago() as $pago) {
                        $fecha_vencimiento_string= $pago->getFechaPago();
                        $fecha_vencimiento_string =  $fecha_vencimiento_string->format("d-m-Y");
                        $fecha_vencimiento= $pago->getFechaPago();
                        $anio_vencimiento=   $fecha_vencimiento->format("Y");
                        
                        
                        if($pago->getEstadoPago()!='Facturado' && $fecha_vencimiento < $fecha_hoy &&  $anio_vencimiento=$anio){
                            $fecha_vencimiento = $fecha_vencimiento->format("d-m-Y");
                            $fecha_hoy = $fecha_hoy->format("d-m-Y");
                            $id= $pago->getId();
                            $newPageUrl = $this->generateUrl('verDatosPasantiaCargada', ['id'=>$pago->getPasantia()->getId()]);
                            $this->addFlash('renovacionPasantia',
                                sprintf('Vencimiento de Pago! %s %s <a href="%s"><i class="far fa-eye"></i></a>',$pago->getPasantia()->getNombre(), $fecha_vencimiento_string, $newPageUrl)
                            );
                        }
                    }
                }

             }
        
        }
    }
    /**
     * 
     */
    public function renovacionConvenios($convenio,  $fecha_hoy, $anio)
    {
        if($convenio!=null) {
            foreach ($convenio as $convenios) {
                if($convenios->getEstadoConvenio()=='Activo') {
           
                    $fecha_vencimiento_string= $convenios->getFechaRenovacion();
                    $fecha_vencimiento_string =  $fecha_vencimiento_string->format("d-m-Y");
                    $fecha_vencimiento= $convenios->getFechaRenovacion();
                    $anio_vencimiento=   $fecha_vencimiento->format("Y");
        
                    $fecha_vencimiento -> modify('-15 days');
                
                    
                    if($fecha_vencimiento < $fecha_hoy &&  $anio_vencimiento=$anio){
                        $fecha_vencimiento = $fecha_vencimiento->format("d-m-Y");
                        $fecha_hoy = $fecha_hoy->format("d-m-Y");
                        $id= $convenios->getId();
                        $newPageUrl = $this->generateUrl('modificarFechaRenovacionConvenio', ['id' => $convenios->getId()]);
                        $this->addFlash('pagoVencido',
                            sprintf('Convenio a punto de vencer! %s %s <a href="%s"><i class="far fa-calendar-alt"></i></a>',$convenios->getEmpresa()->getNombre(), $fecha_vencimiento_string, $newPageUrl)
                        );
                    }
                }

             }
        
        }
    }



    
    


}
